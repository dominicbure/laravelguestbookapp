

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <hr />
                    <h4>edit Post</h4>
                    <form method="post" action="{{ route('post.update', $post->id) }}">
                        <div class="form-group">
                            @csrf
                            @method('PATCH')
                            <label class="label">Edit Title: </label>
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" value="{{ $post->title }}"/>
                        </div>
                        <div class="form-group">
                        <textarea type="hidden" name="body" >{{ $post->body }}</textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-warning" value="Save Updates" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection