@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <p><b>{{ $post->title }}</b></p>
                    <p>
                        {{ $post->body }}
                    </p>
                    <hr />
                    <h4>Display Comments</h4>
                    @foreach($post->replies as $reply)
                        <div class="display-comment">
                            <strong>{{ $reply->user->name }}</strong>
                            <p>{{ $reply->body }}</p>
                        </div>
                    @endforeach
                    <hr />
                    <h4>Add Reply</h4>
                    <form method="post" action="{{ route('reply.add') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="comment_body" class="form-control" />
                            <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-warning" value="Add Reply" />
                        </div>
                        <div class="form-group">
                         <a  href="/posts"><input class="btn btn-primary" value="Back to Posts"></a>
                         </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection