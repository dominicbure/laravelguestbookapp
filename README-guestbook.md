## This project uses docker-compose, I have listed a step-by-step on how to get this project up and running
## this project also uses the latest laravel version 6 install

1. RUN `docker run --rm -v $(pwd):/app composer install`

##  set permissions on the project directory so that it is owned by your non-root user:

2. RUN `sudo chown -R $USER:$USER ~/laravel-guestbook-app`

3. The docker-compose.yml configures the webserver, db and app environment.

4. The Dockerfile installs the dependencies and changes file permissions to run this project 

5. RUN `docker-compose up -d`

## use the following command to list all of the running containers ie app, webserver, and db:

6. docker ps

## run the following command to see that its using the guestbook DB:

7. RUN `docker-compose exec app vim .env`

8. RUN `docker-compose exec app php artisan key:generate`

9. RUN `docker-compose exec app php artisan config:cache`

## for frontend scaffolding
10. RUN `docker-compose exec app composer require laravel/ui`

## for auth implementation in laravel 6

11. RUN `docker-compose exec app php artisan ui vue --auth`

## to compile the JS and css for the auth views

12. RUN `docker-compose exec app npm install && npm run dev`

## Added test users with one as admin and one a normal user Admin==2 normal User==1
email: admin@test.com password:admin; email: user@test.com password:secret

## To create a new user, execute an interactive bash shell on the db container with docker-compose exec:

13. RUN `docker-compose exec db bash`

## Inside the container, log into the MySQL root administrative account:

14. RUN `mysql -u root -plaravel` 
15. RUN `use guestbook;`
16. RUN `GRANT ALL ON guestbook_db.* TO 'laraveluser'@'%' IDENTIFIED BY 'laraveluser';`
17. RUN `FLUSH PRIVILEGES;`

## you can exit the container

## then after all this, running migrations should populate your DB

18. RUN `docker-compose exec app php artisan migrate`
