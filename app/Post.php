<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
     //fields that will interact directly with a user
     protected $fillable = [
        'title',
        'body',
        'user_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class,  'user_id', 'id');
    }

    public function replies()
    {
        return $this->morphMany(Reply::class, 'commentable')->whereNull('parent_id');
    }
}
