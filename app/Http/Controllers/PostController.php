<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller
{
    //
    public function index()
    {

        if(Auth::user() && Auth::user()->role == 1) {
            $posts = Post::where('user_id', Auth::user()->id)->get();
        } else {
            $posts = Post::all();
        }

        return view('index', compact('posts'));
    }

    public function create()
    {
        return view('post');
    }

    public function store(Request $request)
    {
        // store code

        $request->validate([
            'title'=>'required',
            'body'=>'required',
        ]);

        $post = new Post([
            'user_id' => Auth::user()->id,      
            'title' => $request->get('title'),
            'body' => $request->get('body'),
        ]);
        
        $post->save();
        return redirect('/posts')->with('success', 'Post saved!');
    }

    public function show($id)
    {
        $post = Post::find($id);

        return view('show', compact('post'));
    }

    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post->delete();

        return redirect('/posts')->with('success', 'Post deleted!');
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('edit', compact('post'));
    }
    
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'title'=>'required',
            'body'=>'required',
        ]);

        $post = Post::find($id);
        $post->title =  $request->get('title');
        $post->body = $request->get('body');
        $post->save();
        return redirect('/posts')->with('success', 'Post updated!');
    }
}
