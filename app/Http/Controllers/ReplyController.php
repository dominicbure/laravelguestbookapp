<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reply;
use App\Post;

class ReplyController extends Controller
{
    //
    public function store(Request $request)
    {
        $reply = new Reply();
        $reply->body = $request->get('comment_body');
        $reply->user()->associate($request->user());
        $post = Post::find($request->get('post_id'));
        $post->replies()->save($reply);

        return back();
    }

    public function replyStore(Request $request)
    {
        $reply = new Reply();
        $reply->body = $request->get('comment_body');
        $reply->user()->associate($request->user());
        $reply->parent_id = $request->get('comment_id');
        $post = Post::find($request->get('post_id'));

        $post->replies()->save($reply);

        return back();

    }
}
