<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class,  'user_id', 'id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class, 'parent_id');
    }
}
